import irc from 'irc';

const CHANNEL = '#stratum0';

const PW = 'Fagee9ie';

module.exports = function(io) {
  const client = new irc.Client('bouncer.ksal.de', 'infodisplay', {
    channels: [CHANNEL],
    port: 28921,
    secure: true,
    selfSigned: true,
    userName: 'infodisplay/Freenode',
    password: PW,
  });
  const content = [];
  client.addListener('message', (from, to, message) => {
    if (to !== CHANNEL || from === undefined) {
      return;
    }
    // eslint-disable-next-line
    message = message.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    content.push(`<p><span>${from}</span> ${message}</p>`);
    if (content.length > 25) {
      content.shift();
    }
    io.emit('irc.inner', content.join(''));
  });
  io.on('connect', sock => {
    sock.emit(
      'irc',
      '<h3>&nbsp;IRC #stratum0</h3><div class="chat" data-infodisplay-outlet="inner"></div>'
    );
    setTimeout(
      () => {
        sock.emit('irc.inner', content.join(''));
      },
      3000
    );
  });
};
